#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *c;
    c = fopen("homelistings.csv", "r");
    if (!c) {
        printf("Can't open file\n");
    }
    char address[40];
    int zipcode, ID, price, numbeds, numbaths, area, minprice, maxprice, avg, count = 0;
    long total = 0;
    while (fscanf(c, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &ID, address, &price, &numbeds, &numbaths, &area) != EOF) {
        if (count == 0) {
            minprice = price;
            maxprice = price;
        } else {
            if (price < minprice) {
                minprice = price;
            } else if (price > maxprice) {
                maxprice = price;
            }
        }
        total += price;
        count++;
    }
    avg = total / count;
    printf("%d %d %d\n", minprice, maxprice, avg);
    fclose(c);
}