#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *sourcefile, *small, *medium, *large;
    sourcefile = fopen("homelistings.csv", "r");
    small = fopen("smallhomes.txt", "w");
    medium = fopen("mediumhomes.txt", "w");
    large = fopen("largehomes.txt", "w");
    char address[40];
    int zipcode, ID, price, numbeds, numbaths, area;
    while (fscanf(sourcefile, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &ID, address, &price, &numbeds, &numbaths, &area) != EOF) {
        if (area < 1000) {
            fprintf(small, "%s : %d\n", address, area);
        } else if (area >= 1000 && area <= 2000) {
            fprintf(medium, "%s : %d\n", address, area);
        } else if (area >= 2000) {
            fprintf(large, "%s : %d\n", address, area);
        }
    }
    fclose(sourcefile);
}