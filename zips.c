#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *c;
    c = fopen("homelistings.csv", "r");
    if (!c) {
        printf("Can't open file\n");
    }
    char address[40];
    int zipcode, ID, price, numbeds, numbaths, area;
    while (fscanf(c, " %d,%d,%[^,],%d,%d,%d,%d", &zipcode, &ID, address, &price, &numbeds, &numbaths, &area) != EOF) {
        
        char filename[10];
        sprintf(filename, "%d.text", zipcode);
        
        FILE *zipfilename = fopen(filename, "a");
        if (!zipfilename) {
            printf("Can't open file: %s\n", filename);
        }
        fprintf(zipfilename, "%s\n", address);
        fclose(zipfilename);
    }
    fclose(c);
}